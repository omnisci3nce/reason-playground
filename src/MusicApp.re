open VDOM;

// UI Components

// STATE
type song = {
  title: string,
  fileURL: string,
  artist: string,
  trackLength: int
}

let dummySong = {
  title: "", fileURL: "", artist: "", trackLength: 0
}

type model = {
  nowPlaying: int,
  isPlaying: bool,
  progress: float,
  songs: list(song)
};

// type appState =
// | Loading
// | Loaded(model)
// | Failure;

// type transition = (appState => appState);

type action =
  | Play
  | Pause
  | NextSong
  | PrevSong
  | AddSong(song)
  | DeleteSong(int)
  | Toggle // testing purposes

let addSong = (song: song, songs: list(song)) => [song, ...songs];
let deleteSong = index => Belt.List.filterWithIndex(_, (_, i) => index != i);

let nextSong = (np, songs) =>
  np+1;

/* takes an action and returns new state */
let update = (state: model) =>
  fun
  | Play => {...state, isPlaying: true}
  | Pause => {...state, isPlaying: false}
  | NextSong => {...state, nowPlaying: nextSong(state.nowPlaying, state.songs)}
  | PrevSong => state
  | AddSong(song) => {...state, songs: addSong(song, state.songs)}
  | DeleteSong(id) => {...state, songs: deleteSong(id, state.songs)}
  | Toggle => {...state, isPlaying: !state.isPlaying}

let mySongs = [
  { title: "Ghost", fileURL: "assets/Krewella-Ghost.mp3", artist: "Krewella", trackLength: 10 },
  { title: "DIAMOND", fileURL: "assets/OZI-DIAMOND.mp3", artist: "OZI", trackLength: 10 },
];

// Initial State
let init = _ =>
  {
    nowPlaying: 0,
    isPlaying: false,
    progress: 0.,
    songs: mySongs
  };

/* Convenience functions */
let div = createElement("div");
let p = createElement("p");
let audio = createElement("audio");
let audioSource = src => createElement("source", [("src", src)], []); // we can fill in a lot of stuff because source tags shouldn't have children

/* Components */
let header = createElement("h1", [("class", "header")],[createText("Playlist")]);
let currentSongText = (artist, trackName) => createElement("p", [], [createText( artist ++ " - " ++ trackName )]);
let songList = (songs, isPlaying) =>
  div([("class", "song-list " ++ (isPlaying ? "move-right" : ""))],
    List.map(song => {
      div([], [p([], [createText(song.title)])]);
    }, songs)
  );
let audioPlayer = (song, isPlaying) => 
  audio([("controls", "")], [audioSource(song.fileURL)]);


let createVApp = state => {
  open Belt.List;
  let { songs, nowPlaying, progress, isPlaying } = state; // destructure model
  let currentSong = switch (get(songs, nowPlaying)) {
    | Some(song) => song
    | None => dummySong
  };

  let tree = createElement(
    "div",
    [ ("id", "app") ],
    [
      header,
      currentSongText(currentSong.artist, currentSong.title),
      audioPlayer(currentSong, isPlaying),
      songList(state.songs, state.isPlaying)
    ]
  );
  tree;
}

// Initial Mount+Render
let appState = ref(init());

let vApp = ref(createVApp(appState^));
let shadowApp = ref(createVApp(appState^)); // will use later

let app = render(vApp^);
let app2 = render(vApp^);
let rootEl = mount(app, getElById("app"));
let rootEl2 = mount(app2, getElById("app2"));

Js.log(vApp^);
Js.log(computeTotalNodes(vApp^));

/* prettyPrint(vApp^); */

let intervalId = Js.Global.setInterval(() => {
  // appState := update(appState^, Toggle);
  let vNewApp = createVApp(appState^);
  let patch = diff(vApp^, Some(vNewApp));
  patch(rootEl);
  vApp := vNewApp;

}, 1000);