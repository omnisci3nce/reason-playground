/* Types */
type attribute = (string, string);

type domRect = {
    x: float,
    y: float,
    width: float,
    height: float,
    top: float,
    bottom: float
  }

type elementNode = {
  tagName: string,
  attrs: list(attribute),
  children: list(vNode)
}
and vNode =
  | ElementNode(elementNode)
  | TextNode(string);

type runtimeObject = {
  root: vNode,
  shadow: vNode,
  greeting: string,
}

/* Virtual Nodes can be either an Element (with children), or Text (plain string) */

/* Virtual DOM constructors */
let createElement = (tag: string, attrs, children): vNode =>
  ElementNode({
    tagName: tag,
    attrs: attrs,
    children: children
  });

let createText = text => TextNode(text);
/* --------------*/

/* DOM calls */
  [@bs.val] external document : Dom.document = "document" ;
  /* DANGEROUS spooky spooky raw JS stuff (TODO: Figure out how to write type-safe bindings (if that's a thing?)) */
  let setAttr = (el, key: string, value: string) => [%bs.raw {| el.setAttribute(key, value) |}];
  let appendChild = (el, child) => [%bs.raw {| el.appendChild(child) |}];
  let replaceWith = (node, target) => [%bs.raw {| target.replaceWith(node) |}];
  let getElById = (id: string) => [%bs.raw {| document.getElementById(id) |}];
  let createDOMElement = tagName => [%bs.raw {| document.createElement(tagName) |}];
  let createTextNode = text => [%bs.raw {| document.createTextNode(text) |}];
/* --------------*/

/* VDOM->DOM Rendering */
let renderText = text => {
  let el = createTextNode(text);
  el;
}

let rec render = (node: vNode) => {
  switch node {
  | TextNode(text) => createTextNode(text)
  | ElementNode(vNode) => renderElement(vNode)
  };
}
and renderElement = (node: elementNode) => {
  let el = createDOMElement(node.tagName); // calls raw JS / returns dom element (pointer?)
  // Imperative loops that are mutating DOM in JS
  for (i in 0 to (List.length(node.attrs) - 1)) {
    let (key, value) = List.nth(node.attrs, i);
    setAttr(el, key, value); // calls raw JS
  }

  for (i in 0 to (List.length(node.children) - 1)) {
    let childVNode = List.nth(node.children, i);
    let childDomNode = render(childVNode);
    appendChild(el, childDomNode); // calls raw JS
  }

  el;
};

let mount = (node, target) => {
  replaceWith(node,target);
  node;
};
/* --------------*/

/* Diff + Patch */

let removeNode = () => 1;


let diffAndPatchAttrs = (oldAttrs: list(attribute), newAttrs: list(attribute)) => el => {
  // use imperatives loops
  // set new attributes patches
  for (i in 0 to (List.length(newAttrs) - 1)) {
    let (key, value) = List.nth(newAttrs, i);
    /* Js.log(key ++ value ++ string_of_int(i)); */
    setAttr(el, key, value);
    ();
  };
  // remove old attributes that aren't in newAttrs patches
  for (i in 0 to (List.length(oldAttrs) - 1)) {
    let (key, _) = List.nth(newAttrs, i);
    let inNewAttrs = List.exists(((newKey, _)) => {
      key == newKey
    }, newAttrs);
    if (!inNewAttrs) {
      [%bs.raw {| el.removeAttribute(key) |}];
    }
    ();
  };
  el;
};

let genReplacePatch = newNode => node => {
  replaceWith(newNode, node);
  newNode;
}
let genRemovePatch = node => {
  [%bs.raw {| node.remove() |}];
  node; // may return undefined at runtime?
}
// let genComparisonPatch = 

let identity = a => {
  /* Js.log("Hello  from identity"); */
  a;
}

let rec diff = (oldVTree: vNode, newVTree: option(vNode)): (Dom.element => Dom.element) => {
  switch (oldVTree, newVTree) {
    /* node no longer exists so remove */
    | (_, None) => genRemovePatch;

    /* two text nodes : if text same return dummy patch, else replace old with new */
    | (TextNode(t1), Some(TextNode(t2) as newText)) => t1 == t2 ? identity : genReplacePatch(render(newText));

    /* one is a text node, the other an element node */
    | (TextNode(_), Some(ElementNode(_) as newTree))
    | (ElementNode(_), Some(TextNode(_) as newTree)) => genReplacePatch(render(newTree));

    | (ElementNode(n1), Some(ElementNode(n2) as newTree)) when n1.tagName != n2.tagName => genReplacePatch(render(newTree));
    /* (A) */
    | (ElementNode(oldVTree), Some(ElementNode(newVTree))) => parent => {
      diffAndPatchAttrs(oldVTree.attrs, newVTree.attrs, parent); // expecting a node to be sent in
      diffAndPatchChildren(oldVTree.children, newVTree.children, parent);
      parent;
    };
  };
}
and diffAndPatchChildren = (oldVChildren, newVChildren): (Dom.element => Dom.element) => {
    switch (List.length(oldVChildren), List.length(newVChildren)) {
      // lengths are the same
      | (oldCs, newCs) when oldCs == newCs => (parent) => {
        for (i in 0 to (List.length(oldVChildren) - 1)) {
          let childDomNode = [%bs.raw {| parent.childNodes[i] |}];
          let old = List.nth(oldVChildren, i);
          let _new = List.nth(newVChildren, i);
          let patch = diff(old, Some(_new));
          patch(childDomNode);
          ();
        };
        parent;
      };

      /* | (oldCs, newCs) when oldCs > newCs => (parent) => {
        for (i in 0 to List.length(oldVChildren)) {
          /* diff(List.nth(oldVChildren, i), Some(List.nth(oldVChildren, i)), parent); */
          ();
        };
        parent;
      }; */
      | (oldCs, newCs) when oldCs > newCs => identity;

      | (oldCs, newCs) when oldCs < newCs => identity; 
      | (_, _) => identity;
    }
};

/* Helpers */
let rec summarise = (l: list(int)) =>
  switch l {
  | [] => 0
  | [head, ...tail] => head + summarise(tail)
  };

let rec computeTotalNodes = (t: vNode) => {
  switch t {
    | TextNode(_) => 1
    | ElementNode(node) when List.length(node.children) == 0 => 1
    | ElementNode(node) => 1 +
                          summarise(
                            List.map(computeTotalNodes, node.children)
                          )
  }
};


let rec _prettyPrint = node => {
  switch node {
    | TextNode(s) => print_string("Text")
    | ElementNode(n) when List.length(n.children) == 0 => {
      print_string("Element");
      print_newline();
    }
    | ElementNode(n) => {
      print_string("Element");
      print_newline();
      for (i in 0 to (List.length(n.children) - 1)) {
        _prettyPrint(List.nth(n.children, i));
        print_newline();
      }
    }
  }
  
}

let prettyPrint = (root: vNode) => {
  Format.open_box(0);

  _prettyPrint(root);

  // Format.print_flush();

  Format.close_box();
}