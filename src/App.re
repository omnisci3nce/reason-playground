open VDOM;

/* My application! */
let myImage = createElement(
  "img",
  [
    ("id", "image"),
    ("src", "https://i.pinimg.com/originals/7c/65/97/7c65977e88a3b18ee6efe42cd4e256be.gif")
  ],
  []
);
let p = (text, colour) => createElement("p", [("style", "color: " ++ colour)], [createText(text)]);
let countText = count => p("Count: " ++ string_of_int(count), "#333333");

let createVApp = count =>
  createElement(
    "div",
    [ ("id", "app"), ("dataCount", string_of_int(count)) ],
    /* Children below */
    [   countText(count), // plain text
        p("Cool pixelart animation I found online!", Utils.getRandomColor()),
        myImage
    ]
  );

// STATE
let count = ref(0);

// Initial Mount+Render
let vApp = ref(createVApp(count^));
let app = render(vApp^);
let rootEl = mount(app, getElById("app"));

// log vdom to console for debugging
Js.log(vApp);



let intervalId = Js.Global.setInterval(() => {
  count := count^ + 1;
  let vNewApp = createVApp(count^);
  let patch = diff(vApp^, Some(vNewApp));
  patch(rootEl);
  vApp := vNewApp;

}, 1000);