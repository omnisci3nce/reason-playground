open Tea.App;
open Tea.Html;


/* MODEL */
let init = _ => false;

/* UPDATE */
type msg =
  | ToggleMenu;

let update = model =>
  fun
  | ToggleMenu => !model;

/* VIEW */
let view = model =>
  div(
    [],
    [
      div(
        [model ? class'("open") : class'("closed")],
        [text("menu")]
      ),
      text(model ? "Open" : "Closed"),
      button([onClick(ToggleMenu)], [text("Toggle Menu")])
    ]
  );

let main = beginnerProgram({model: init(), update, view});
