Js.log("Hello, BuckleScript and Reason!");

let rec _decimalToBinary =
    fun
    | 0 => []
    | n => [n mod 2, ...(_decimalToBinary(n / 2))];
let decimalToBinary = x => _decimalToBinary(x) |> List.rev;

let intsToString = xs =>
    List.fold_left(
        (a, b) => a ++ string_of_int(b),  /* first argument is our function (acc, val) => newAccumulator */
        "",                               /* accumulator */
        xs                                /* array of our integers */
    );

let printDecimalAsBinary = x => decimalToBinary(x) |> intsToString;

Js.log(printDecimalAsBinary(1));
Js.log(printDecimalAsBinary(2));
Js.log(printDecimalAsBinary(5));
Js.log(printDecimalAsBinary(12));
Js.log(printDecimalAsBinary(100));
Js.log(printDecimalAsBinary(105));
Js.log(printDecimalAsBinary(1234));
Js.log(printDecimalAsBinary(13798));
Js.log(printDecimalAsBinary(203873434));